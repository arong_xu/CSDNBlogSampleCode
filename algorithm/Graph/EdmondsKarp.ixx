module;
#include <fmt/format.h>
#include <fmt/ranges.h>

#include <algorithm>
#include <queue>
#include <string>
#include <tuple>
#include <vector>

export module Graph.EdmondsKarp;
import Graph.AdjList;
import Graph.Interface;

export namespace graph {
class EdmondsKarp {
 public:
  explicit EdmondsKarp(const AdjList& graph) : graph_(graph) {}

  std::vector<WeightedEdge> FindArgumentPath(const AdjList& graph, unsigned src,
                                             unsigned dst) {
    std::vector<unsigned> parent(graph.V(), UINT_MAX);
    std::vector<bool> visited(graph.V(), false);

    std::queue<unsigned> q;
    q.push(src);
    while (!q.empty()) {
      auto curr = q.front();
      q.pop();

      if (curr == dst) break;
      if (visited[curr]) continue;
      visited[curr] = true;

      for (auto w : graph.Adj(curr)) {
        if (visited[w]) continue;
        if (graph.GetWeight(curr, w) <= 0) continue;
        parent[w] = curr;
        q.push(w);
      }
    }
    std::vector<WeightedEdge> path;
    if (parent[dst] == UINT_MAX) return path;
    int curr = dst;
    while (parent[curr] != src) {
      auto begin = parent[curr];
      auto end = curr;
      auto weight = graph.GetWeight(begin, end);
      path.emplace_back(begin, end, weight);
      curr = begin;
    }
    path.emplace_back(src, curr, graph.GetWeight(src, curr));
    std::reverse(path.begin(), path.end());
    return path;
  }

  int MaxFlow(unsigned source, unsigned sink) {
    BuildResidualGraph();

    int max_flow = 0;
    while (true) {
      auto path = FindArgumentPath(residual_graph_, source, sink);
      fmt::println("path: {}\n", fmt::join(path, ","));

      if (path.empty()) break;
      auto it = std::min_element(path.begin(), path.end(),
                                 [](const auto& lhs, const auto& rhs) {
                                   return std::get<2>(lhs) < std::get<2>(rhs);
                                 });
      auto flow = std::get<2>(*it);
      max_flow += flow;

      for (auto& [u, v, w] : path) {
        residual_graph_.UpdateWeight(u, v, -flow);
        residual_graph_.UpdateWeight(v, u, flow);
      }
    }
    return max_flow;
  }

  void BuildResidualGraph() {
    residual_graph_.Reset(graph_.V(), graph_.Directed(), graph_.Weighted());
    for (Vertex u = 0; u < graph_.V(); u++) {
      for (Vertex v : graph_.Adj(u)) {
        residual_graph_.AddEdge(u, v, graph_.GetWeight(u, v));
        residual_graph_.AddEdge(v, u, 0);
      }
    }
  }

  std::string ResidualGraph() const {
    std::string out;
    out.append("digraph G {\n");
    out.append("\trankdir=LR;\n");
    for (auto u = 0; u < graph_.V(); u++) {
      for (const auto& v : graph_.Adj(u)) {
        out.append("\t");
        out.append(std::to_string(u));
        out.append(" -> ");
        out.append(std::to_string(v));

        out.append("[label=\"");
        out.append(std::to_string(residual_graph_.GetWeight(v, u)));
        out.append("/");
        out.append(std::to_string(v));

        out.append("\"];\n");
      }
    }
    out.append("}\n");
    return out;
  }

 private:
  const AdjList& graph_;
  AdjList residual_graph_;
};
}  // namespace graph
