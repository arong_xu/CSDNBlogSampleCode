// bipartite_test.cpp
#include <gtest/gtest.h>

#include "Bipartite.ixx"

using namespace graph;

TEST(ColorDFSTest, SimpleBipartiteGraph) {
  // 创建一个简单的二分图
  // 0 -- 1 -- 2 -- 3
  Graph g(4);
  g.AddEdge(0, 1);
  g.AddEdge(1, 2);
  g.AddEdge(2, 3);

  std::vector<Color> colors(g.V(), Color::Unknown);
  bool result = ColorDFS(g, colors, 0, Color::Red);

  EXPECT_TRUE(result);
  EXPECT_EQ(colors[0], Color::Red);
  EXPECT_EQ(colors[1], Color::Black);
  EXPECT_EQ(colors[2], Color::Red);
  EXPECT_EQ(colors[3], Color::Black);
}

TEST(ColorDFSTest, NonBipartiteGraph) {
  // 创建一个非二分图
  // 0 -- 1 -- 2
  // |    |
  // 3 -- 4
  Graph g(5);
  g.AddEdge(0, 1);
  g.AddEdge(1, 2);
  g.AddEdge(0, 3);
  g.AddEdge(1, 4);
  g.AddEdge(3, 4);

  std::vector<Color> colors(g.V(), Color::Unknown);
  bool result = ColorDFS(g, colors, 0, Color::Red);

  EXPECT_FALSE(result);
}

TEST(ColorDFSTest, SingleVertexGraph) {
  // 创建一个只有一个顶点的图
  Graph g(1);

  std::vector<Color> colors(g.V(), Color::Unknown);
  bool result = ColorDFS(g, colors, 0, Color::Red);

  EXPECT_TRUE(result);
  EXPECT_EQ(colors[0], Color::Red);
}

TEST(ColorDFSTest, DisconnectedGraph) {
  // 创建一个不连通的图
  // 0 -- 1    2 -- 3
  Graph g(4);
  g.AddEdge(0, 1);
  g.AddEdge(2, 3);

  std::vector<Color> colors(g.V(), Color::Unknown);
  bool result = ColorDFS(g, colors, 0, Color::Red);

  EXPECT_TRUE(result);
  EXPECT_EQ(colors[0], Color::Red);
  EXPECT_EQ(colors[1], Color::Black);
  EXPECT_EQ(colors[2], Color::Unknown);
  EXPECT_EQ(colors[3], Color::Unknown);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
