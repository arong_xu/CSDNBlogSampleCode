#include <fmt/core.h>
#include <fmt/ranges.h>

#include <iostream>
#include <tuple>
#include <vector>

using std::make_tuple;

import Graph.AdjList;
import Graph.EdmondsKarp;
import Graph.Dinic;
import Graph.Interface;

int main() {
  {
    std::vector<graph::WeightedEdge> edges{
        make_tuple(0, 1, 5), make_tuple(0, 2, 8), make_tuple(1, 3, 3),
        make_tuple(1, 4, 4), make_tuple(2, 4, 3), make_tuple(2, 5, 4),
        make_tuple(3, 6, 2), make_tuple(4, 6, 4), make_tuple(5, 6, 5),
    };
    graph::AdjList wg(7, edges, true);
    graph::EdmondsKarp ek(wg);
    auto len = ek.MaxFlow(0, 6);
    std::cout << "max flow: " << len << "\n";
    std::cout << ek.ResidualGraph();

    graph::Dinic dc(wg);
    auto l2 = dc.MaxFlow(0, 6);
    std::cout << "dinic max flow: " << l2 << "\n";
  }

  {
    std::vector<graph::WeightedEdge> edges = {
        std::make_tuple(0, 1, 3), std::make_tuple(0, 2, 2),
        std::make_tuple(1, 2, 5), std::make_tuple(1, 3, 2),
        std::make_tuple(2, 3, 3),
    };
    graph::AdjList wg(4, edges, true);
    graph::EdmondsKarp ek(wg);
    auto len = ek.MaxFlow(0, 3);
    std::cout << "max flow: " << len << "\n";
    std::cout << ek.ResidualGraph();

    graph::Dinic dc(wg);
    auto l2 = dc.MaxFlow(0, 3);
    std::cout << "dinic max flow: " << l2 << "\n";
  }

  return 0;
}
