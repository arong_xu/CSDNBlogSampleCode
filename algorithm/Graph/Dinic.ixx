module;
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <algorithm>
#include <queue>
#include <vector>

export module Graph.Dinic;
import Graph.AdjList;
import Graph.Interface;

export namespace graph {
class Dinic {
 public:
  explicit Dinic(const AdjList& graph)
      : graph_(graph), residual_graph_(graph.V(), true, true) {
    BuildResidualGraph();
  }

  int MaxFlow(int src, int dst) {
    int max_flow = 0;
    while (true) {
      auto level = BuildLevelGraph(src);
      auto path = FindArgumentPath(level, src, dst);
      fmt::println("current path: {}", fmt::join(path, ", "));

      if (path.empty()) {
        break;
      }
      auto it = std::min_element(path.begin(), path.end(),
                                 [](const auto& lhs, const auto& rhs) {
                                   return std::get<2>(lhs) < std::get<2>(rhs);
                                 });
      int flow = std::get<2>(*it);
      if (flow <= 0) {
        break;
      }
      max_flow += flow;
      for (auto& [u, v, w] : path) {
        residual_graph_.UpdateWeight(u, v, -flow);
        residual_graph_.UpdateWeight(v, u, flow);
      }
    }
    return max_flow;
  }
  void BuildResidualGraph() {
    for (Vertex u = 0; u < graph_.V(); u++) {
      for (Vertex v : graph_.Adj(u)) {
        residual_graph_.AddEdge(u, v, graph_.GetWeight(u, v));
        residual_graph_.AddEdge(v, u, 0);
      }
    }
  }
  AdjList BuildLevelGraph(unsigned src) {
    AdjList g(graph_.V(), true, true);
    std::queue<unsigned> q;
    q.push(src);
    while (!q.empty()) {
      auto u = q.front();
      q.pop();
      for (auto v : residual_graph_.Adj(u)) {
        int w = residual_graph_.GetWeight(u, v);
        if (w <= 0 || g.HasEdge(u, v)) {
          continue;
        }
        g.AddEdge(u, v, w);
        q.push(v);
      }
    }
    return g;
  }

  std::vector<WeightedEdge> FindArgumentPath(const AdjList& graph, unsigned src,
                                             unsigned dst) {
    std::vector<unsigned> parent(graph.V(), UINT_MAX);
    std::vector<bool> visited(graph.V(), false);

    std::queue<unsigned> q;
    q.push(src);
    while (!q.empty()) {
      auto curr = q.front();
      q.pop();

      if (curr == dst) break;
      if (visited[curr]) continue;
      visited[curr] = true;

      for (auto w : graph.Adj(curr)) {
        if (visited[w]) continue;
        if (graph.GetWeight(curr, w) <= 0) continue;
        parent[w] = curr;
        q.push(w);
      }
    }
    std::vector<WeightedEdge> path;
    if (parent[dst] == UINT_MAX) return path;
    int curr = dst;
    while (parent[curr] != src) {
      auto begin = parent[curr];
      auto end = curr;
      auto weight = graph.GetWeight(begin, end);
      path.emplace_back(begin, end, weight);
      curr = begin;
    }
    path.emplace_back(src, curr, graph.GetWeight(src, curr));
    std::reverse(path.begin(), path.end());
    return path;
  }

 private:
  const AdjList& graph_;
  AdjList residual_graph_;
};
}  // namespace graph
