#include <vector>
#include <queue>

using namespace std;

class Solution {
  bool ColorDFS(vector<vector<int>>& graph, std::vector<int>& colors, int start,
                int color) {
    colors[start] = color;
    for (auto v : graph[start]) {
      if (colors[v] == color) return false;
      if (colors[v] == -1) {
        if (!ColorDFS(graph, colors, v, 1 - color)) {
          return false;
        }
      }
    }
    return true;
  }

  bool ColorBFS(const vector<vector<int>>&  graph, std::vector<int>& colors,
                    int start, int color) {
    std::queue<int> q;
    q.push(start);
    colors[start] = color;

    while (!q.empty()) {
      int v = q.front();
      q.pop();
      auto opposite_color = 1 - colors[v];

      for (auto w : graph[v]) {
        // Found an edge connecting two vertices of the same color
        if (colors[w] == colors[v]) {
          return false;
        }
        if (colors[w] == -1) {
          colors[w] = opposite_color;
          q.push(w);
        }
      }
    }
    return true;
  }

public:
  bool isBipartite(vector<vector<int>>& graph) {
    vector<int> colors(graph.size(), -1);
    for (int i = 0; i < graph.size(); ++i) {
      if (colors[i] == -1) {
        if (!ColorBFS(graph, colors, i, 0)) {
          return false;
        }
      }
    }
    return true;
  }
};
