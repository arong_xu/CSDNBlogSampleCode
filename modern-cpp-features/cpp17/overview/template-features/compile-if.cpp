#include <iostream>
#include <list>
#include <string>
#include <type_traits>
#include <vector>

template <typename T>
std::string asString(T x) {
  if constexpr (std::is_same_v<T, std::string>) {
    return x;  // 直接返回字符串类型
  } else if constexpr (std::is_arithmetic_v<T>) {
    return std::to_string(x);  // 转换为字符串
  } else {
    return std::string(x);  // 尝试转换为字符串
  }
}

template <typename Callable, typename... Args>
decltype(auto) call(Callable op, Args&&... args) {
  if constexpr (std::is_void_v<std::invoke_result_t<Callable, Args...>>) {
    // 返回类型是 void
    op(std::forward<Args>(args)...);
    // 执行其他操作...
    return;
  } else {
    // 返回类型不是 void
    decltype(auto) ret{op(std::forward<Args>(args)...)};
    // 执行其他操作...
    return ret;
  }
}

template <typename Iterator, typename Distance>
void Advance(Iterator& pos, Distance n) {
  using cat = std::iterator_traits<Iterator>::iterator_category;
  if constexpr (std::is_same_v<cat, std::random_access_iterator_tag>) {
    pos += n;
  } else if constexpr (std::is_same_v<cat, std::bidirectional_iterator_tag>) {
    if (n >= 0) {
      while (n--) {
        ++pos;
      }
    } else {
      while (n++) {
        --pos;
      }
    }
  } else {  // input_iterator_tag
    while (n--) {
      ++pos;
    }
  }
}

int main() {
  std::cout << asString(42) << '\n';                    // 输出: 42
  std::cout << asString(std::string("hello")) << '\n';  // 输出: hello
  std::cout << asString("hello") << '\n';               // 输出: hello

  // 示例函数
  auto lambda = [](int a, int b) -> int { return a + b; };
  auto void_lambda = [](int a, int b) { /* do nothing */ };

  auto result = call(lambda, 5, 3);  // 结果是 8
  call(void_lambda, 5, 3);           // 返回 void

  std::vector<int> vec{1, 2, 3, 4, 5};
  std::list<int> lst{1, 2, 3, 4, 5};

  auto it_vec = vec.begin();
  auto it_lst = lst.begin();

  Advance(it_vec, 2);  // 使用随机访问迭代器
  Advance(it_lst, 2);  // 使用双向迭代器
}
