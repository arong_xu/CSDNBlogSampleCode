#include <any>
#include <iostream>
#include <string>

int main() {
  std::any anyVal;

  anyVal = 42;
  std::cout << "anyVal contains an int: " << std::any_cast<int>(anyVal)
            << std::endl;

  anyVal = "hello";
  std::cout << "anyVal contains a string: "
            << std::any_cast<const char*>(anyVal) << std::endl;

  // 使用 type() 检查当前存储的类型
  if (anyVal.type() == typeid(int)) {
    std::cout << "anyVal is an int" << std::endl;
  } else if (anyVal.type() == typeid(std::string)) {
    std::cout << "anyVal is a string" << std::endl;
  }

  return 0;
}
