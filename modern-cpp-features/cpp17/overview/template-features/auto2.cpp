#include <iostream>

// 定义一个模板结构体, 使用auto作为模板参数
template <auto v>
struct Constant {
  static constexpr auto value = v;
};

int main() {
  // 使用不同的常量初始化模板
  using IntConstant = Constant<42>;
  using CharConstant = Constant<'x'>;
  using BoolConstant = Constant<true>;

  std::cout << "IntConstant: " << IntConstant::value
            << std::endl;  // 输出: IntConstant: 42
  std::cout << "CharConstant: " << CharConstant::value
            << std::endl;  // 输出: CharConstant: x
  std::cout << "BoolConstant: " << BoolConstant::value
            << std::endl;  // 输出: BoolConstant: 1

  return 0;
}
