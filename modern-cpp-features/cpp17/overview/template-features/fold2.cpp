#include <iostream>
#include <string>

template <typename... T>
std::string foldConcat(T... args) {
  return (... + std::string(args));  // 左折叠
}

int main() {
  // 输出: Concatenated string: hello world!
  std::cout << "Concatenated string: " << foldConcat("hello", " ", "world", "!")
            << std::endl;
}
