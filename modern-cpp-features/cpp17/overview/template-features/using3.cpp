#include <iostream>
#include <string>

template <typename T>
class Base {
  T value{};

 public:
  Base() : value{} {}
  Base(T v) : value{v} {}

  void print() const { std::cout << "Value: " << value << '\n'; }
};

template <typename... Types>
class Multi : private Base<Types>... {
 public:
  // 继承所有基类的构造函数
  using Base<Types>::Base...;

  // 提供一个统一的 print 接口
  void print() const {
    // 使用折叠表达式依次调用每个基类的 print 函数
    (static_cast<Base<Types> const*>(this)->print(), ...);
  }

  // 提供特定类型的 print 函数
  template <typename T>
  void print_specific() const {
    static_cast<Base<T> const*>(this)->print();
  }
};

int main() {
  // 使用Multi<> 类型声明三个不同类型的对象
  using MultiISB = Multi<int, std::string, bool>;

  MultiISB m1 = 42;                    // 调用Base<int>的构造函数
  MultiISB m2 = std::string("hello");  // 调用Base<std::string>的构造函数
  MultiISB m3 = true;                  // 调用Base<bool>的构造函数

  m1.print_specific<int>();          // 输出: Value: 42
  m2.print_specific<std::string>();  // 输出: Value: hello
  m3.print_specific<bool>();         // 输出: Value: 1

  return 0;
}
