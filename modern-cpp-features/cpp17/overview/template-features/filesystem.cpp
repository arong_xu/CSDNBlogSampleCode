#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

int main() {
  // 创建一个目录
  fs::path dirPath = "example_dir";
  if (!fs::exists(dirPath)) {
    fs::create_directory(dirPath);
    std::cout << "Directory created: " << dirPath << std::endl;
  } else {
    std::cout << "Directory already exists: " << dirPath << std::endl;
  }

  // 创建一个文件并写入内容
  fs::path filePath = dirPath / "example.txt";
  // 文件属性
  fs::file_status status = fs::status(filePath);
  std::cout << "File exists: " << fs::exists(status) << std::endl;
  std::cout << "Is regular file: " << fs::is_regular_file(status) << std::endl;
  std::cout << "File size: " << fs::file_size(filePath) << " bytes"
            << std::endl;

  // 检查文件是否存在
  if (fs::exists(filePath)) {
    std::cout << "File exists: " << filePath << std::endl;
  } else {
    std::cout << "File does not exist: " << filePath << std::endl;
  }

  return 0;
}
