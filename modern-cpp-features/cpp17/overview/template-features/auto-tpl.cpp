#include <iostream>

// 定义一个模板类, 使用auto作为非类型模板参数
template <auto N>
class S {
 public:
  static void print() {
    std::cout << "Value: " << N << ", Type: " << typeid(N).name() << std::endl;
  }
};

template <typename T, auto N>
class A {
 public:
  A(const std::array<T, N>&) {}
  A(T (&)[N]) {}
};

int main() {
  // 使用不同类型的常量初始化模板
  S<42> s1;   // int 类型
  S<'a'> s2;  // char 类型

  // S<2.5> s3;  // 错误: double 类型不被支持

  s1.print();  // 输出: Value: 42, Type: i
  s2.print();  // 输出: Value: a, Type: c

  return 0;
}
