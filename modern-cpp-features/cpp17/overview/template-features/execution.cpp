#include <algorithm>
#include <execution>
#include <iostream>
#include <vector>

int main() {
  // 创建一个包含一些整数的向量
  std::vector<int> numbers = {5, 6, 7, 8, 9, 10, 1, 2, 3, 4};

  // 使用并行版本的 std::for_each 对向量中的每个元素进行处理
  std::sort(std::execution::par, numbers.begin(), numbers.end());

  // 输出处理后的向量
  std::cout << "Processed numbers: ";
  for (const auto& num : numbers) {
    std::cout << num << " ";
  }
  std::cout << std::endl;

  return 0;
}
