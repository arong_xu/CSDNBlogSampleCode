#include <iostream>
#include <string>
#include <variant>

int main() {
  std::variant<int, std::string> var;
  auto print = [](auto&& arg) {};
  var = 42;
  std::cout << "var contains an int: " << std::get<int>(var) << std::endl;

  var = "hello";
  std::cout << "var contains a string: " << std::get<std::string>(var)
            << std::endl;

  // 使用 std::holds_alternative 检查当前存储的类型
  if (std::holds_alternative<int>(var)) {
    std::cout << "var is an int" << std::endl;
  } else if (std::holds_alternative<std::string>(var)) {
    std::cout << "var is a string" << std::endl;
  }

  return 0;
}
