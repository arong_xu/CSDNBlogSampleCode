#include <array>
#include <iostream>

template <typename T, auto N>
std::array<T, N> arr{};

template<typename T, auto N>
void printArr(std::array<T, N>& arr) {
  for (const auto& elem : arr) {
    std::cout << elem << ' ';
  }
}

int main() {
  arr<int, 5>[0] = 17;
  arr<int, 10>[0] = 42;
  printArr();
}
