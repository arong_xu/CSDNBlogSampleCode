#include <iostream>

template <typename... T>
auto foldSum(T... args) {
  return (... + args);  // 左折叠
}

int main() {
  std::cout << "Sum: " << foldSum(47, 11, 5, -1) << std::endl;  // 输出: Sum: 62
}
