#include <iostream>
#include <optional>

int main() {
  auto print = [](auto optional) {
    if (optional) {
      std::cout << "optional contains value: " << *optional << std::endl;
    } else {
      std::cout << "optional does not contain a value" << std::endl;
    }
  };

  std::optional<int> optional;  // 空的 optional
  print(optional);

  optional = 42;
  print(optional);

  optional.reset();  // 清空 optional
  print(optional);

  return 0;
}
