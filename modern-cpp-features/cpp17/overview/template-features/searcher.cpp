#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <string>

int main() {
  std::string text = "ABAAABCD";
  std::string pattern = "ABC";

  // default_searcher
  auto p0 = std::search(text.begin(), text.end(),
                        std::default_searcher{pattern.begin(), pattern.end()});

  // boyer_moore_searcher
  auto p1 =
      std::search(text.begin(), text.end(),
                  std::boyer_moore_searcher{pattern.begin(), pattern.end()});

  // boyer_moore_horspool_searcher
  auto p2 = std::search(
      text.begin(), text.end(),
      std::boyer_moore_horspool_searcher{pattern.begin(), pattern.end()});

  return 0;
}
