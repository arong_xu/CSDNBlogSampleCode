#include <array>
#include <iostream>
#include <string>
#include <vector>

int main() {
  std::vector<int> vec = {1, 2, 3, 4, 5};
  std::array<int, 5> arr = {10, 20, 30, 40, 50};
  std::string str = "Hello, World!";

  std::empty(vec);
  std::empty(arr);
  std::empty(str);

  std::size(vec);
  std::size(arr);
  std::size(str);

  std::data(vec);
  std::data(arr);
  std::data(str);

  return 0;
}
