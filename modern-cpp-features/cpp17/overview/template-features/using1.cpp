#include <iostream>

class Base {
 public:
  void a() { std::cout << "Base::a\n"; }
  void b() { std::cout << "Base::b\n"; }
  void c() { std::cout << "Base::c\n"; }
};

class Derived : private Base {
 public:
  // 使用逗号分隔的using声明导入多个基类成员
  using Base::a, Base::b, Base::c;
};

int main() {
  Derived d;
  d.a();  // 输出: Base::a
  d.b();  // 输出: Base::b
  d.c();  // 输出: Base::c
}
