#include <iostream>
#include <string>

// 定义一个结构体模板, 用于"继承"所有函数调用操作符
template <typename... Ts>
struct Overload : Ts... {
  using Ts::operator()...;  // 使用using声明使所有操作符可用
};

// 基类型推导指南
template <typename... Ts>
Overload(Ts...) -> Overload<Ts...>;

int main() {
  // 创建一个包含两个lambda表达式的Overload对象
  auto twice =
      Overload{[](std::string& s) { s += s; }, [](auto& v) { v *= 2; }};

  int i = 42;
  twice(i);                         // 调用第二个lambda
  std::cout << "i: " << i << '\n';  // 输出: i: 84

  std::string s = "hi";
  twice(s);                         // 调用第一个lambda
  std::cout << "s: " << s << '\n';  // 输出: s: hihi
}
