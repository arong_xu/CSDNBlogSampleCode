#include <iostream>
#include <string_view>

void print(std::string_view sv) {
  std::cout << "string view: " << sv << std::endl;
}

int main() {
  std::string normalStr = "This is a test";
  print(normalStr);

  const char* cStr = "Another test";
  print(cStr);

  return 0;
}
