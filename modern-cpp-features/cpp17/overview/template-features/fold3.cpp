#include <iostream>
#include <type_traits>

// 类型特征: 检查一组类型是否相同
template <typename T1, typename... TN>
struct IsHomogeneous {
  static constexpr bool value = (std::is_same_v<T1, TN> && ...);
};

template <typename T1, typename... TN>
constexpr bool isHomogeneous(T1, TN...) {
  return (std::is_same_v<T1, TN> && ...);
}

int main() {
  std::cout << "Is homogeneous? " << IsHomogeneous<int, int, int>::value
            << std::endl;  // 输出: Is homogeneous? 1
  std::cout << "Is homogeneous? " << isHomogeneous(42, 84, 126)
            << std::endl;  // 输出: Is homogeneous? 1
  std::cout << "Is heterogeneous? " << isHomogeneous(42, 84.0, "hello")
            << std::endl;  // 输出: Is heterogeneous? 0
}
