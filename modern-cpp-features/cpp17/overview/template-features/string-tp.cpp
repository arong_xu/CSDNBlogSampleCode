#include <iostream>

// 定义一个模板类, 接受一个常量字符指针作为模板参数
template <const char* str>
class Message {
 public:
  void print() const { std::cout << str << std::endl; }
};

// 定义字符串变量, 分别具有外部链接, 内部链接
extern const char external[] = "Hello, World!";  // 外部链接
const char internal[] = "Hello, Internal!";      // 内部链接

int main() {
  // 使用具有外部链接的字符串, 所有版本都支持
  Message<external> msg_external;  // all version
  msg_external.print();            // 输出: Hello, World!

  // 使用具有内部链接的字符串(C++11及更高版本)
  Message<internal> msg_internal;
  msg_internal.print();  // 输出: Hello, Internal!

  // 使用无链接的字符串(仅在C++17及以上版本支持)
  static const char hello_world_no_link[] = "Hello, No Link!";  // 无链接
  Message<hello_world_no_link> msg_no_link;
  msg_no_link.print();  // 输出: Hello, No Link!

  return 0;
}
