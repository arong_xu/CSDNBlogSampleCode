#include <complex>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

// Non-type template parameter deduction
template <typename T, int SZ>
class MyClass {
 public:
  MyClass(T (&)[SZ]) {}
};

MyClass mc("hello");  // deduces T as const char and SZ as 6

int main() {
  // For containers like std::vector
  std::vector v1{1, 2, 3};           // std::vector<int>
  std::vector v2{"hello", "world"};  // std::vector<const char*>

  // Class Template Argument Deduction for std::complex
  std::complex c1{1.1, 2.2};  // std::complex<double>
  std::cout << "Complex: " << c1 << std::endl;

  // For std::tuple
  std::tuple t{42, 'x', nullptr};  // std::tuple<int, char, std::nullptr_t>

  return 0;
}
