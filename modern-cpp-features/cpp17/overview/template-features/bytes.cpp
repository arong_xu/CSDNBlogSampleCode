#include <cstddef>
#include <iostream>

int main() {
  std::byte b1{0x1F};    // 初始化为 0x1F
  std::byte b2{0b1010};  // 使用二进制初始化

  // 使用 std::to_integer 转换为整数
  int intVal = std::to_integer<int>(b1);
  std::cout << "b1 as int: " << intVal << std::endl;

  // 使用位运算
  std::byte b3 = b1 & b2;
  std::cout << "b1 & b2: " << std::to_integer<int>(b3) << std::endl;

  // 使用 |= 运算符
  b1 |= b2;
  std::cout << "b1 |= b2: " << std::to_integer<int>(b1) << std::endl;

  return 0;
}
