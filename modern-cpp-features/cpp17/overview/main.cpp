#include "main.h"

#include <array>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>

#if __has_include(<filesystem>)
#include <filesystem>
#define HAS_FILESYSTEM 1
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
#define HAS_FILESYSTEM 1
#define FILESYSTEM_IS_EXPERIMENTAL 1
#elif __has_include("filesystem.hpp")
#include "filesystem.hpp"
#define HAS_FILESYSTEM 1
#define FILESYSTEM_IS_EXPERIMENTAL 1
#else
#define HAS_FILESYSTEM 0
#endif

// nested namespaces
// before
namespace Company {
namespace Project {
namespace Component {
class Foo;
}  // namespace Component
}  // namespace Project
}  // namespace Company

// after
namespace Company::Project::Component {
class Foo;
}

// lambda capture *this
class C {
 private:
  std::string name;

 public:
  void foo() {
    auto l1 = [*this] { std::cout << name << '\n'; };
  }
};

// nodiscard
[[nodiscard]] int openFile(const char* path) {
  int fd = -1;
  // ...
  return fd;
}

void comment(int place) {
  switch (place) {
    case 1:
      std::cout << "非常 ";
      [[fallthrough]];
    case 2:
      std::cout << "好\n";
      break;
    default:
      std::cout << "一般\n";
      break;
  }
}

void processData([[maybe_unused]] int value, double factor) {
  // 这里没有使用 value 参数，但由于 [[maybe_unused]] 标记，不会产生警告
  double result = factor * 2;
  std::cout << "Result: " << result << std::endl;
}

int main() {
  // struct binding
  struct Item {
    int id;
    std::string name;
  };

  Item item = {1, "item"};
  auto [id, name] = item;

  std::map<std::string, int> scores = {
      {"Alice", 85}, {"Bob", 90}, {"Charlie", 78}};
  // 绑定std::pair
  for (const auto& [key, value] : scores) {
    std::cout << key << ": " << value << '\n';
  }

  // 绑定tuple
  auto t1 = std::tuple<int, int, int>{1, 2, 3};
  auto [x, y, z] = t1;

  // if 语句中初始化变量
  if (auto it = scores.find("Bob"); it != scores.end()) {
    std::cout << "找到了元素 Bob" << std::endl;
  }

  // 在 switch 语句中初始化变量
  switch (auto score = scores["Bob"]; score / 10) {
    case 9:
      std::cout << "Bob 的成绩等级是 A" << std::endl;
      break;
    case 8:
      std::cout << "Bob 的成绩等级是 B" << std::endl;
      break;
    case 7:
      std::cout << "Bob 的成绩等级是 C" << std::endl;
      break;
    default:
      std::cout << "Bob 的成绩等级未知" << std::endl;
  }

  // -----
  struct Data {
    std::string name;
    double value;
  };
  struct MoreData : Data {
    bool done;
  };
  MoreData md1{{"test1", 6.778}, false};  // 使用嵌套括号进行初始化
  MoreData md2{"test1", 6.778,
               false};  // 当基类或子对象只接受一个值时，可省略嵌套括号

  // -----

  // ----- mandatory copy elision
  class MCE {
    int x;

   public:
    MCE() { std::cout << "Constructor called" << std::endl; }
    // 显式删除复制构造函数
    MCE(const MCE&) = delete;
    // 显式删除移动构造函数
    MCE(MCE&&) = delete;
    ~MCE() { std::cout << "Destructor called" << std::endl; }
  };
  auto createObject = []() { return MCE(); };

  MCE mce1 = MCE();           // 直接初始化
  MCE mce2 = createObject();  // 返回临时对象

  // lambda
  auto l1 = [](auto x) constexpr { return x * x; };  // OK
  constexpr int ci1 = l1(2);

  // u8 char literals
  char c = u8'a';          // ASCII
  char16_t ch = u'猫';     // 中文字符
  char32_t emoji = U'🍌';  // emoji

  int i = 0;
  std::array<int, 2> a = {1, 2};

  i = ++i + 2;            // well-defined
  i = i++ + 2;            // undefined behavior until C++17
  a[i] = i++;             // undefined behavior until C++17
  std::cout << i << i++;  // undefined behavior until C++17
  i = ++i + i++;          // undefined behavior
  int n = ++i + i;        // undefined behavior

  class Base {
   public:
    virtual void foo() noexcept;
  };
  class Derived : public Base {
   public:
    // void foo() override;  // ERROR: does not override
  };

  //   // 无作用域枚举，指定底层类型为char
  //   enum Enum1 : char {};
  //   Enum1 i1{42};     // OK since C++17 (ERROR before C++17)
  //   Enum1 i2 = 42;    // 仍然错误
  //   Enum1 i3(42);     // 仍然错误
  //   Enum1 i4 = {42};  // 仍然错误

  //   // 有作用域枚举，默认底层类型
  //   enum class Enum2 { mon, tue, wed, thu, fri, sat, sun };
  //   Enum2 s1{0};     // OK since C++17 (ERROR before C++17)
  //   Enum2 s2 = 0;    // 仍然错误
  //   Enum2 s3(0);     // 仍然错误
  //   Enum2 s4 = {0};  // 仍然错误

  //   // 有作用域枚举，指定底层类型为char
  //   enum class Enum3 : char { mon, tue, wed, thu, fri, sat, sun };
  //   Enum3 s5{0};  // OK since C++17 (ERROR before C++17)

  //   Enum3 s6 = 0;    // 仍然错误
  //   Enum3 s7(0);     // 仍然错误
  //   Enum3 s8 = {0};  // 仍然错误

  //   // 无作用域枚举，未指定底层类型
  //   enum Enum4 { bit1 = 1, bit2 = 2, bit3 = 4 };
  //   Enum4 f1{0};  // 仍然错误

  //   // 尝试进行窄化转换，仍然错误
  //   enum Enum5 : char {};
  //   Enum5 i5{42.2};  // 仍然错误

  auto at1{42};  // 类型为int
  // auto at2{1, 2, 3};  // Error
  return 0;
}
