#pragma once

class sample {
  inline static int default_value = 47; /* C++ 17支持内联定义, 无需分开初始化 */
};

inline int global_uuid = 0; /* C++ 17新特性, 被多个文件include也不会出现冲突 */
