#include <iostream>
#include <semaphore>
#include <thread>
#include <vector>

std::counting_semaphore<5> sem(5);  // 指定模板参数类型为 int

void worker(int id) {
  sem.acquire();  // 尝试获取信号量
  std::cout << "Worker " << id << " is working\n";
  std::this_thread::sleep_for(std::chrono::seconds(1));  // 模拟工作
  std::cout << "Worker " << id << " is done\n";
  sem.release();  // 释放信号量
}

int main() {
  std::vector<std::thread> threads;
  for (int i = 0; i < 10; ++i) {
    threads.emplace_back(worker, i);
  }

  for (auto& t : threads) {
    t.join();
  }

  return 0;
}
