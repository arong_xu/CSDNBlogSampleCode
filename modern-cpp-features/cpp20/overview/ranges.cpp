#include <fmt/ranges.h>

#include <algorithm>
#include <iostream>
#include <ranges>
#include <vector>

int main() {
  std::vector<int> vec = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  // 使用views过滤出偶数, 并对每个元素加1
  auto result = vec | std::views::filter([](int n) { return n % 2 == 0; }) |
                std::views::transform([](int n) { return n * n; });

  // 打印结果
  fmt::println("{}", result);
  // 输出: [0, 4, 16, 36, 64]

  return 0;
}
