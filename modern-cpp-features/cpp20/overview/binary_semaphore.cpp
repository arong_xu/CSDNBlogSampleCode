#include <iostream>
#include <semaphore>
#include <thread>
#include <vector>

std::binary_semaphore sem(0);  // 初始状态为0, 表示资源不可用

void worker(int id) {
  sem.acquire();  // 尝试获取信号量
  std::cout << "Worker " << id << " is working\n";
  std::this_thread::sleep_for(std::chrono::seconds(1));  // 模拟工作
  std::cout << "Worker " << id << " is done\n";
}

void producer() {
  std::this_thread::sleep_for(std::chrono::seconds(2));  // 模拟生产时间
  std::cout << "Producer is ready\n";
  sem.release();  // 释放信号量, 表示资源可用
}

int main() {
  std::thread t1(worker, 1);
  std::thread t2(producer);

  t1.join();
  t2.join();

  return 0;
}
