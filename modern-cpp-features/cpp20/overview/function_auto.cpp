#include <iostream>

auto add(auto a, auto b) { return a + b; }

int main() {
  std::cout << "Integers: " << add(1, 2) << "\n";     // 输出3
  std::cout << "Doubles: " << add(1.5, 2.5) << "\n";  // 输出4.0

  return 0;
}
