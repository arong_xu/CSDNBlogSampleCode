export module math;

export int add(int a, int b) { return a + b; }

export int multiply(int a, int b) { return a * b; }
