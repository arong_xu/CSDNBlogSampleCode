constexpr int square(int x) { return x * x; }
consteval int triple(int x) { return x * 3; }

int main() {
  constexpr int CX_VAL = square(5);  // 编译期计算
  // CX_VAL = 1;                     // 错误, 常量不可修改
  int val = square(5);  // 初始化普通变量
  square(val);          // OK, 可以在运行时计算

  constinit static int value = 10;  // 在编译期初始化, 但不是常量
  value = 20;
  // constinit int value2 = 10; // 错误，constinit 只能用于静态变量或者全局变量

  const int data = triple(4);  // 合法，因为在编译期求值
  // triple(runtime);          // 错误，不能在在运行期求值

  return 0;
}
