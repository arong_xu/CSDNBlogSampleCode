#include <concepts>
#include <string>

// 定义一个名为Sortable的概念, 要求类型T支持小于运算符
template <typename T>
concept Sortable = requires(T a, T b) {
  { a < b } -> std::convertible_to<bool>;
};

// 仅对Sortable类型的参数有效
void sortFunction(Sortable auto& container) {
  // 假设这里实现了排序逻辑
}

struct MyType {
  int value;
  bool operator<(const MyType& other) const { return value < other.value; }
};

int main() {
  int arr[] = {1, 3, 2};
  sortFunction(arr);  // 正确, int支持<运算

  MyType myArr[] = {{1}, {3}, {2}};
  sortFunction(myArr);  // 正确, MyType也重载了<运算符

  return 0;
}
