#include <compare>
#include <iostream>

struct Point {
  int x, y;

  auto operator<=>(const Point&) const = default;
};

int main() {
  Point a{1, 2}, b{1, 2}, c{2, 3};

  if (a <=> b == 0) {
    std::cout << "a and b are equal\n";
  }

  if ((a <=> c) < 0) {
    std::cout << "a is less than c\n";
  }

  return 0;
}
