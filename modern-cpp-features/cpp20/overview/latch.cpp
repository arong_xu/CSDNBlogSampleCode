#include <iostream>
#include <latch>
#include <thread>
#include <vector>

void worker(std::latch& latch) {
  // 模拟工作
  std::this_thread::sleep_for(std::chrono::seconds(1));
  std::cout << "Worker thread done\n";
  latch.count_down();
}

int main() {
  std::latch latch(3);  // 等待3个线程

  std::vector<std::thread> threads;
  for (int i = 0; i < 3; ++i) {
    threads.emplace_back(worker, std::ref(latch));
  }

  latch.wait();  // 主线程等待所有工作线程完成
  std::cout << "All worker threads are done\n";

  for (auto& t : threads) {
    t.join();
  }

  return 0;
}
