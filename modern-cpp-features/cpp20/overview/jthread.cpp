#include <chrono>
#include <iostream>
#include <stop_token>
#include <thread>

void worker(std::stop_token stopToken) {
  while (!stopToken.stop_requested()) {
    std::cout << "Working...\n";
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  std::cout << "Stopped by request.\n";
}

int main() {
  // 创建一个 jthread，并传递 worker 函数
  std::jthread t(worker);

  // 主线程等待 3 秒
  std::this_thread::sleep_for(std::chrono::seconds(3));

  // 请求线程停止
  t.request_stop();

  // jthread 析构时会自动 join，确保线程完成执行
  return 0;
}
