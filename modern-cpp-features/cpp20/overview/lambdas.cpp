#include <fmt/core.h>
#include <fmt/ranges.h>

int main() {
  // 1. 泛型 Lambda
  auto generic = []<typename T>(T a, T b) { return a + b; };

  fmt::println("Generic Lambda (int): {}", generic(1, 2));         // 输出: 3
  fmt::println("Generic Lambda (double): {}", generic(1.5, 2.5));  // 输出: 4.0

  // 2. `constexpr` Lambda
  constexpr auto constexprLambda = [](int a, int b) constexpr { return a + b; };

  constexpr int result = constexprLambda(3, 4);
  fmt::println("Constexpr Lambda: {}", result);  // 输出: 7

  // 3. `[*this]` 捕获
  struct MyStruct {
    int value = 42;
    auto getLambda() {
      return [*this]() { return value; };
    }
  };

  MyStruct obj;
  auto lambda = obj.getLambda();
  obj.value = 100;
  fmt::println("Lambda with [*this] capture: {}", lambda());  // 输出: 42

  return 0;
}
