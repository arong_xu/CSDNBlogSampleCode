#include <cassert>
#include <iostream>
#include <string>

int main() {
  std::string str = "Hello, World!";

  // 检查字符串是否以 "Hello" 开头
  assert(str.starts_with("Hello"));

  // 检查字符串是否以 '!' 结尾
  assert(str.ends_with('!'));

  return 0;
}
