#include <iostream>
#include <string_view>

// 使用浮点类型作为非类型模板参数
template <double Value>
void print() {
  std::cout << "Double value: " << Value << "\n";
}

int main() {
  // 浮点类型模板参数
  print<3.14>();  // 输出: Double value: 3.14

  return 0;
}
