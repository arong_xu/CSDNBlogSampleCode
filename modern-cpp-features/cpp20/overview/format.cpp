#include <format>
#include <iostream>

int main() {
  int num = 42;
  double pi = 3.14159;
  std::string name = "Alice";

  // 基本格式化
  std::cout << std::format("Number: {}, Pi: {:.2f}, Name: {}\n", num, pi, name);
  // 输出: Number: 42, Pi: 3.14, Name: Alice

  // 格式化字符串存储到变量
  std::string formatted =
      std::format("Number: {}, Pi: {:.2f}, Name: {}", num, pi, name);
  std::cout << formatted << std::endl;
  // 输出: Number: 42, Pi: 3.14, Name: Alice

  // 格式化字符串中的位置参数
  std::cout << std::format("Name: {2}, Pi: {1:.2f}, Number: {0}\n", num, pi,
                           name);
  // 输出: Name: Alice, Pi: 3.14, Number: 42

  return 0;
}
