#include <fmt/ranges.h>

#include <span>
#include <vector>

void print(std::span<int> span) { fmt::println("{}", span); }

int main() {
  // 使用 std::span 查看数组
  int arr[] = {1, 2, 3, 4, 5};
  print(arr);  // 输出: 1 2 3 4 5

  // 使用 std::span 查看 vector
  std::vector<int> vec = {6, 7, 8, 9, 10};
  print(vec);  // 输出: 6 7 8 9 10

  // 使用 std::span 查看部分数据
  std::span<int> partial(vec.data() + 1, 3);
  print(partial);  // 输出: 7 8 9

  return 0;
}
