import math;
#include <iostream>

int main() {
  std::cout << "Add: " << add(2, 3) << "\n";            // 输出: Add: 5
  std::cout << "Multiply: " << multiply(2, 3) << "\n";  // 输出: Multiply: 6
  return 0;
}
