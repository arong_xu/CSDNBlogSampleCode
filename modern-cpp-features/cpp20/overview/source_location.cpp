#include <fmt/core.h>

#include <source_location>

int main() {
  auto sl = std::source_location::current();
  fmt::println("file: {}", sl.file_name());
  fmt::println("function: {}", sl.function_name());
  fmt::println("line/col: {}/{}", sl.line(), sl.column());

  return 0;
}
