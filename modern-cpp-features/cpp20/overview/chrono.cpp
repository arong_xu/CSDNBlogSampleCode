#include <chrono>
#include <format>
#include <iostream>

int main() {
  using namespace std::chrono;

  // 1. 创建日历日期
  year_month_day today = 2025y / March / 6d;
  std::cout << "Today is: " << today << std::endl;  // 输出: 2025-03-06

  // 2. 创建时间点
  sys_time<seconds> sys_now = time_point_cast<seconds>(system_clock::now());
  std::cout << "System time now: " << sys_now << std::endl;  // 输出当前系统时间

  // 3. 时区转换
  auto utc_time = sys_now;
  auto local_time = zoned_time{current_zone(), utc_time};
  std::cout << "Local time now: " << local_time
            << std::endl;  // 输出当前本地时间

  // 4. 计算两个日期之间的天数
  year_month_day next_week = sys_days{today} + days{7};
  std::cout << "Next week is: " << next_week << std::endl;  // 输出: 2025-03-13

  // 5. 格式化输出
  std::cout << std::format("Today is {:%Y-%m-%d}\n",
                           today);  // 输出: Today is 2025-03-06

  return 0;
}
