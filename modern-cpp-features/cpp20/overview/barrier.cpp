#include <barrier>
#include <iostream>
#include <thread>
#include <vector>

void worker(std::barrier<>& barrier, int id) {
  std::cout << "Worker " << id << " is ready\n";
  barrier.arrive_and_wait();  // 等待所有线程到达
  std::cout << "Worker " << id << " is processing\n";
  barrier.arrive_and_wait();  // 等待所有线程完成处理
  std::cout << "Worker " << id << " is done\n";
}

int main() {
  std::barrier barrier(3);  // 等待3个线程

  std::vector<std::thread> threads;
  for (int i = 0; i < 3; ++i) {
    threads.emplace_back(worker, std::ref(barrier), i);
  }

  for (auto& t : threads) {
    t.join();
  }

  return 0;
}
