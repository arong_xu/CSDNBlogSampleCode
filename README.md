# 博客代码仓库

这个仓库是我用来存放博客帖子中用到的示例代码, 方便读者下载.

欢迎关注: [阿荣的博客](https://www.arong-xu.com/)

## 目录

### C++ 开发环境搭建系列

- [VS Code Dev Containers 教程: 从基础到进阶配置](https://www.arong-xu.com/posts/vscode-dev-containers-complete-guide/)
- [Windows 11 CLion C++ Dev Container 开发环境配置](https://www.arong-xu.com/posts/clion-dev-containers-complete-guide/)
- [VS Code C++ 开发环境配置](https://www.arong-xu.com/posts/configure-vscode-as-cpp-ide/)
- [Vcpkg 使用全攻略: 支持 VS Code, Visual Studio 和 CLion](https://www.arong-xu.com/posts/cpp-package-management-vcpkg-from-zero-to-hero/)
- [CMake 入门教程: 从基础到实践](https://www.arong-xu.com/cmake-column/hands-on-cmake/)

### Modern C++ 系列

- [C++17 新特性总结](https://www.arong-xu.com/posts/modern-cpp/cpp17-new-features-overview/)
- [C++23 新特性总结](https://www.arong-xu.com/posts/modern-cpp/cpp23-new-features-overview/)
- [C++26 新特性预览](https://www.arong-xu.com/posts/cpp26-new-features-preview/)

### 算法系列

- [图论](algorithm/Graph/readme.md)

### C++ 核心指导原则(Core Guidelines)

- [哲学部分](https://www.arong-xu.com/posts/cpp-core-guidelines/philosophy/)
- [接口(Interface)部分](https://www.arong-xu.com/posts/cpp-core-guidelines/interface/)
- [函数部分](https://www.arong-xu.com/posts/cpp-core-guidelines/functions/)
- [类和类层次结构部分](https://www.arong-xu.com/posts/cpp-core-guidelines/class-and-class-hierarchy/)
- [枚举部分](https://www.arong-xu.com/posts/cpp-core-guidelines/enumerations/)
- [资源管理部分](https://www.arong-xu.com/posts/cpp-core-guidelines/resource-management/)
- [表达式和语句部分](https://www.arong-xu.com/posts/cpp-core-guidelines/expressions-and-statements/)
- [性能部分](https://www.arong-xu.com/posts/cpp-core-guidelines/performance/)
- [错误处理](https://www.arong-xu.com/posts/cpp-core-guidelines/error-handling/)
- [常量和不可变性](https://www.arong-xu.com/posts/cpp-core-guidelines/constants-and-immutability/)
- [泛型编程](https://www.arong-xu.com/posts/cpp-core-guidelines/generic-programming/)

## 提示

1. 部分代码可能需要特定编译器才能通过, 请参考博客里面的环境设置.
2. 请在克隆代码的时候使用浅克隆(shallow clone), 用来加快克隆速度:

   - gitcode.com:
     ```bash
     git clone --depth=1 https://gitcode.com/arong_xu/BlogSampleCode.git
     ```
   - github.com:
     ```bash
     git clone --depth=1 https://github.com/arong/BlogSampleCode.git
     ```

   后续如果有需求需要完整历史, 可以用如下命令:

   ```cpp
   git fetch --unshallow
   ```

3. 有意见或者建议, 或者遇到问题了请 [创建 issue](https://gitcode.com/arong_xu/BlogSampleCode/issues/create)
